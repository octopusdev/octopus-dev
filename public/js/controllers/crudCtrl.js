var crudCtrl = angular.module('crudCtrl', []);
crudCtrl.controller('crudCtrl', function($scope, $http){
    //DUMMY DATA
    $scope.rowCollection = [
     {first_name: 'Laurent', last_name: 'Renard', age: 22, gender: 'Men', address: 'Jakarta', email: 'whatever@gmail.com'},
     {first_name: 'Blandine', last_name: 'Faivre', age: 19, gender: 'Woman', address: 'Kuningan', email: 'oufblandou@gmail.com'},
     {first_name: 'Francoise', last_name: 'Frere', age: 45, gender: 'Shemale', address: 'bandung', email: 'raymondef@gmail.com'}
     ];

    // Initializes Variables
    // ----------------------------------------------------------------------------
    $scope.formData = {};

    // Creates a new user based on the form fields
    $scope.createUser = function() {
        // Grabs all of the text box fields
        var userData = {
            first_name: $scope.formData.first_name,
            last_name: $scope.formData.last_name,
            age: $scope.formData.age,
            gender: $scope.formData.gender,
            address: $scope.formData.address,
            email: $scope.formData.email
        };

        // Saves the user data to the db
        $http.post('/users', userData)
            .success(function (data) {
                // Once complete, clear the form (except location)
                $scope.formData.first_name = "";
                $scope.formData.last_name= "";
                $scope.formData.age= "";
                $scope.formData.gender= "";
                $scope.formData.address= "";
                $scope.formData.email= "";
            })
            .error(function (data) {
                console.log('Error: ' + data);
            });
    };

  /*  $scope.rowCollection = [];

    $http.get('/users',data)
            .success(function (res) {
                // Once complete, clear the form (except location)
                /!*$scope.first_name = data.first_name;
                 $scope.last_name= data.last_name;
                 $scope.age= data.age;
                 $scope.gender= data.gender;
                 $scope.address= data.address;
                 $scope.email= data.email;*!/
                rowCollection = data;
                console.log(data);
            });*/
});