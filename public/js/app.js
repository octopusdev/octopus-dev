// Declares the initial angular module "meanMapApp". Module grabs other controllers and services. Note the use of ngRoute.
var app = angular.module('meanMapApp', ['addCtrl','crudCtrl','geolocation', 'gservice','testCtrl','testService', 'ngRoute'])

    // Configures Angular routing -- showing the relevant view and controller when needed.
    .config(function($routeProvider){

        // Join Team Control Panel
        $routeProvider.when('/map', {
            controller: 'addCtrl',
            templateUrl: 'partials/addForm.html',

            // Find Teammates Control Panel
        }).when('/crud', {
            controller : 'crudCtrl',
            templateUrl: 'partials/crud.html',
        }).when('/test_page',{
            controller: 'testCtrl',
            templateUrl: 'partials/testPage.html',
        })
            // All else forward to the Join Team Control Panel
        .otherwise({redirectTo:'/#'})
    });